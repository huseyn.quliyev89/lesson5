package az.ingress.car.service;

import az.ingress.car.model.Car;

import java.math.BigDecimal;
import java.util.List;

public interface CarService {
    Car createCar(Car car);

    Car updateCar(Integer carId, Car car);

    List<Car> getAllCar();

    Car getCarById(Integer carId);

    void deleteCar(Integer carId);

    List<Car> getCarByModel(String carModel);

    List<Car> getCarByPrice(BigDecimal carPrice);

    Integer getCarsCount();
}
