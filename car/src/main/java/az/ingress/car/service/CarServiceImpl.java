package az.ingress.car.service;

import az.ingress.car.model.Car;
import az.ingress.car.repository.CarRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class CarServiceImpl implements CarService {

    CarRepository carRepository;
    @Value("${car.makername}")
    private String makerName;

    public CarServiceImpl(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Override
    public Car createCar(Car car) {
        car.setMakerName(makerName);
        return carRepository.save(car);
    }

    @Override
    public Car updateCar(Integer carId, Car car) {
        Optional<Car> carDb = carRepository.findById(car.getId());

        if (carDb.isPresent()) {
            Car carUpdate = carDb.get();
            carUpdate.setId(car.getId());
            carUpdate.setModel(car.getModel());
            carUpdate.setMakePlace(car.getMakePlace());
            carUpdate.setColor(car.getColor());
            carUpdate.setMakeYear(car.getMakeYear());
            carUpdate.setPrice(car.getPrice());
            carUpdate.setIsElectric(car.getIsElectric());
            return carRepository.save(carUpdate);
        } else {
            throw new RuntimeException("Record not found with id : " + carId);
        }
    }

    @Override
    public List<Car> getAllCar() {
        return carRepository.findAll();
    }

    @Override
    public Car getCarById(Integer carId) {
        Optional<Car> car = carRepository.findById(carId);

        if (car.isPresent()) {
            return car.get();
        } else {
            throw new RuntimeException("Record not found with id : " + carId);
        }
    }

    @Override
    public void deleteCar(Integer carId) {
        carRepository.deleteById(carId);
    }

    @Override
    public List<Car> getCarByModel(String carModel) {
        return carRepository.findByModel(carModel);
    }

    @Override
    public List<Car> getCarByPrice(BigDecimal carPrice) {
        return carRepository.findByPrice(carPrice);
    }

    @Override
    public Integer getCarsCount() {
        return carRepository.countCars();
    }
}
