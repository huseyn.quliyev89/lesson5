package az.ingress.car.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;

import java.math.BigDecimal;
import java.time.LocalDate;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table()
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "model")
    private String model;
    @Column(name = "makeplace")
    private String makePlace;
    @Column(name = "color")
    private String color;
    @Column(name = "makeyear")
    private LocalDate makeYear;
    @Column(name = "price")
    private BigDecimal price;
    @Column(name = "iselectric")
    private Boolean isElectric;
    @Column(name = "makername")
    private String makerName;
}
