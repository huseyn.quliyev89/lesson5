package az.ingress.car.controller;

import az.ingress.car.model.Car;
import az.ingress.car.service.CarService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/v1/car")
public class CarController {

    CarService carService;

    public CarController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping
    public List<Car> getAllCar() {
        return carService.getAllCar();
    }

    @PostMapping
    public Car createCar(@RequestBody Car car) {
        return carService.createCar(car);
    }

    @PutMapping("/{id}")
    public Car updateCar(@PathVariable Integer id, @RequestBody Car car) {
        car.setId(id);
        return carService.updateCar(id, car);
    }

    @GetMapping("/{carId}")
    public Car getCarById(@PathVariable Integer carId) {

        return carService.getCarById(carId);
    }

    @GetMapping("/getcarbymodel/{carModel}")
    public List<Car> getCarByModel(@PathVariable String carModel) {

        return carService.getCarByModel(carModel);
    }

    @GetMapping("/getcarbyprice/{carPrice}")
    public List<Car> getCarByModel(@PathVariable BigDecimal carPrice) {
        return carService.getCarByPrice(carPrice);
    }

    @GetMapping("/getcarscount")
    public Integer getCarsCount() {
        return carService.getCarsCount();
    }

    @DeleteMapping("{carId}")
    public void deleteCar(@PathVariable Integer carId) {
        carService.deleteCar(carId);
    }
}
